package cmd

import (
	"log"
	"os"

	pi "codeberg.org/ssmns/shekan-go/cmd/pip"
	sh "codeberg.org/ssmns/shekan-go/cmd/shekan"
	"github.com/spf13/cobra"
)

var version = "0.2.4"
var RootCmd = &cobra.Command{
	Use:     "shekan",
	Version: version,
	Short:   "shekan - a simple CLI to change Linux Based Os DNS | pip mirrors",
	Long: `shekan is a super fancy CLI (kidding)
   
One can use shekan to modify your DNS setting from the terminal.
To make shekan accessible to all users, including the root user, simply place it in the system bin after downloading.(/usr/bin)`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Println("Run shekan -h or shekan --help to see help menu.")
	},
}

func Execute() {
	shekan_cmds := sh.SubCmd()
	for _, s := range shekan_cmds {
		RootCmd.AddCommand(s)
	}
	pip_cms := pi.SubCmd()
	for _, s := range pip_cms {
		RootCmd.AddCommand(s)
	}
	if err := RootCmd.Execute(); err != nil {

		log.Printf("Whoops. There was an error while executing your CLI '%s'", err)
		os.Exit(1)
	}
}
