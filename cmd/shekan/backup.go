package shekan

import (
	"codeberg.org/ssmns/shekan-go/pkg/shekan"
	"codeberg.org/ssmns/shekan-go/pkg/utils"
	"github.com/spf13/cobra"
)

var backupResolv = &cobra.Command{
	Use:     "backup",
	Aliases: []string{"bck"},
	Short:   "Backup of resolve.conf",
	Example: "shekan backup init|backup",
	Args:    cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		utils.MustBeRoot()
		shekan.Backup()
	},
}
