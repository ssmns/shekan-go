package shekan

import (
	"codeberg.org/ssmns/shekan-go/pkg/shekan"
	"github.com/spf13/cobra"
)

var showCurrent = &cobra.Command{
	Use:     "show",
	Aliases: []string{"sh"},
	Short:   "Show /etc/resolve.conf contents",
	Example: "shekan show",
	Args:    cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		shekan.Show()
	},
}
