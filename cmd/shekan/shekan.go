package shekan

import "github.com/spf13/cobra"

func SubCmd() []*cobra.Command {

	return []*cobra.Command{backupResolv, changeResolv, restoreResolv, showCurrent}

}
