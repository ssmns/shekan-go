package shekan

import (
	"codeberg.org/ssmns/shekan-go/pkg/shekan"
	"codeberg.org/ssmns/shekan-go/pkg/utils"
	"github.com/spf13/cobra"
)

var changeResolv = &cobra.Command{
	Use:           "change",
	Aliases:       []string{"ch"},
	Short:         "Change DNS's",
	Example:       "shekan change shecan|begzar|google|403|hostiran|dnspro|electrotm",
	Args:          cobra.ExactArgs(1),
	ValidArgs:     []string{"shecan", "begzar", "google", "403", "hostiran", "dnspro", "electrotm"},
	SilenceErrors: true,
	Run: func(cmd *cobra.Command, args []string) {

		utils.MustBeRoot()
		if utils.IsFileExisted("/etc/resolv.conf.bck") {
			shekan.Restore("bck")
		}
		shekan.Backup()
		shekan.Change(args[0])

	},
}
