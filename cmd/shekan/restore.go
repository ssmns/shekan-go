package shekan

import (
	"log"

	bt "codeberg.org/ssmns/shekan-go/pkg/beutify"
	"codeberg.org/ssmns/shekan-go/pkg/shekan"
	"codeberg.org/ssmns/shekan-go/pkg/utils"

	"github.com/spf13/cobra"
)

var restoreResolv = &cobra.Command{
	Use:           "restore",
	Aliases:       []string{"re"},
	Short:         "Restore to default DNS's",
	Example:       "shekan restore init|backup",
	Args:          cobra.ExactArgs(1),
	SilenceErrors: true,
	ValidArgs:     []string{"init", "backup", "bck", "back"},
	Run: func(cmd *cobra.Command, args []string) {
		utils.MustBeRoot()
		state, res := shekan.Restore(args[0])
		if res {
			log.Printf("%s  resolv.conf restore to %s %s'%s' %s\n ", bt.GetColor("green"), bt.GetColor("none"), bt.GetColor("orange"), state, bt.GetColor("none"))
		}
	},
}
