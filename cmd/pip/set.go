package pip

import (
	p "codeberg.org/ssmns/shekan-go/pkg/pip"
	"codeberg.org/ssmns/shekan-go/pkg/utils"
	"github.com/spf13/cobra"
)

var changePip = &cobra.Command{
	Use:           "setpip",
	Aliases:       []string{"sp"},
	Short:         "Change pip proxy",
	Long:          `[flags] are: china_01 to china_07`,
	Args:          cobra.ExactArgs(1),
	ValidArgs:     []string{"china_01", "china_02", "china_03", "china_04", "china_05", "china_06", "china_07"},
	Example:       "shekan setpip china_01",
	SilenceErrors: true,
	Run: func(cmd *cobra.Command, args []string) {
		utils.MustNotBeRoot()
		p.ChangePipProxy(args[0])

	},
}
