package pip

import (
	"log"

	bt "codeberg.org/ssmns/shekan-go/pkg/beutify"
	p "codeberg.org/ssmns/shekan-go/pkg/pip"

	"codeberg.org/ssmns/shekan-go/pkg/utils"
	"github.com/spf13/cobra"
)

var restorePip = &cobra.Command{
	Use:           "unsetpip",
	Aliases:       []string{"usp"},
	Short:         "restore default pip proxy",
	Args:          cobra.ExactArgs(0),
	ValidArgs:     []string{"unsetpip", "usp"},
	Example:       "shekan unsetpip",
	SilenceErrors: true,
	Run: func(cmd *cobra.Command, args []string) {
		utils.MustNotBeRoot()
		filename := "/home/" + utils.GetUser() + "/.config/pip/pip.conf"
		if utils.ContainsWord(filename, "[global]") && utils.ContainsWord(filename, "proxy") {
			p.RestorePipProxy()
		}
		log.Printf("%sWarning:%s There is no global.proxy to unset.\n", bt.GetColor("orange"), bt.GetColor("none"))
	},
}
