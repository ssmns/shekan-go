package pip

import (
	"github.com/spf13/cobra"
)

func SubCmd() []*cobra.Command {

	return []*cobra.Command{changePip, restorePip}

}
