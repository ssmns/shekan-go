# shekan-go

The "shekan-go" program is designed to modify the default DNS server on Debian-based operating systems. Additionally, it has the ability to adjust the pip proxy server to optimize the speed of downloading Python packages.


Available DNS server:

- [google](http://google.com)
- [begzar](https://begzar.ir/)
- [shecan](http://shecan.ir)
- [403](https://403.online/)
- [hostiran](https://hostiran.net/landing/proxy)
- [electrotm](https://electrotm.org/)
- [dnspro](https://dnspro.ir/)

Available Pypi.org mirrors:

- [china_01](https://pypi.tuna.tsinghua.edu.cn/simple/)
- [china_02](https://mirrors.aliyun.com/pypi/simple/ )
- [china_03](https://pypi.mirrors.ustc.edu.cn/simple/)
- [china_04](https://repo.huaweicloud.com/repository/pypi/simple/)
- [china_05](http://pypi.douban.com/simple/)
- [china_06](http://pypi.sdutlinux.org/)
- [china_07](http://pypi.hustunique.com/)
