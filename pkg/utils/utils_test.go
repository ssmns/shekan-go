package utils

import (
	"fmt"
	"strconv"
	"testing"
)

type containsWordTest struct {
	filename string
	word     string
	expected bool
}

var containsWordTests = []containsWordTest{
	{"example.txt", "BRYANT", true},
	{"example.txt", "BROWNI", false},
	{"example.txt", "TAYLOR", true},
	{"example.txt", "WHITe", false},
	{"example.txt", "YoUNG", false},
	{"example.txt", "COLE", true},
}

func TestContainsWord(t *testing.T) {
	for _, test := range containsWordTests {
		if output := ContainsWord(test.filename, test.word); output != test.expected {
			t.Errorf("Output %q not equal to expected %q", strconv.FormatBool(output), strconv.FormatBool(test.expected))
		}
	}
}

func BenchmarkContainsWord(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ContainsWord("example.txt", "BRYANT")
	}
}

func ExampleContainsWord() {
	fmt.Println(ContainsWord("example.txt", "BRYANT"))
	// Output: true
}
