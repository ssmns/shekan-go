package utils

import (
	"bufio"
	"log"
	"os"
	"os/user"
	"strings"

	bt "codeberg.org/ssmns/shekan-go/pkg/beutify"
	"github.com/manifoldco/promptui"
)

func GetUser() string {
	currentUser, err := user.Current()
	if err != nil {
		log.Fatalf("[isRoot] Unable to get current user: %s", err)
	}
	return currentUser.Username
}

func MustBeRoot() {
	if GetUser() != "root" {
		log.Fatalf("%s Error:%s Shekan need root permistion to change in resolv.conf. Please run with 'sudo' or a sudoer user.\n ", bt.GetColor("red"), bt.GetColor("none"))
	}
}

func MustNotBeRoot() {
	if GetUser() == "root" {
		log.Fatalf("%s Error:%s Shekan not permited to change pip with root permistion. Please run without 'sudo'.\n ", bt.GetColor("red"), bt.GetColor("none"))
	}
}

func ContainsWord(filename string, word string) bool {
	file, err := os.Open(filename)
	if err != nil {
		log.Println(err)
		return false
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if scanner.Text() == word {
			return true
		}
	}

	if err := scanner.Err(); err != nil {
		log.Println(err)
		return false
	}

	return false
}

func GetPrompt() bool {
	prompt := promptui.Select{
		Label: "Continue[Yes/No]?",
		Items: []string{"Yes", "No"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatalf("Prompt failed %v\n", err)
	}
	return result == "Yes"
}

func IsFileExisted(filename string) bool {
	// Check if file exists
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

func ReplaceStringInFile(filename, searchStr, replaceStr string) error {
	data, err := os.ReadFile(filename)
	if err != nil {
		return err
	}

	lines := strings.Split(string(data), "\n")

	for i, line := range lines {
		if strings.Contains(line, searchStr) {
			lines[i] = strings.Replace(line, searchStr, replaceStr, -1)
		}
	}

	output := strings.Join(lines, "\n")
	err = os.WriteFile(filename, []byte(output), 0644)
	if err != nil {
		return err
	}

	return nil
}

func Backupfile() {

}
func AddLineToFile() {

}
func RemoveLineFromFile() {

}
