package docker

import (
	"fmt"
)

func AptAddFodevProxy(url string) {
	target := "/etc/apt/apt.conf.d/proxy.conf"
	var body string
	if url == "" {
		body = "Acquire::http::Proxy::download.docker.com \"http://fodev.org:8118/\";"
	} else {
		body = fmt.Sprintf("Acquire::http::Proxy::%s \"http://fodev.org:8118/\";", url)
	}
	fmt.Println(target, body)

}
