package beutify

func GetColor(name string) string {

	data := map[string]interface{}{
		"black":   "\033[30m",
		"red":     "\033[31m",
		"green":   "\033[32m",
		"yellow":  "\033[33m",
		"blue":    "\033[34m",
		"magenta": "\033[35m",
		"cyan":    "\033[36m",
		"white":   "\033[37m",
		"orange":  "\033[93m",
		"none":    "\033[0m",
	}
	return data[name].(string)
}
