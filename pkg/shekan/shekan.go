package shekan

import (
	"fmt"
	"log"
	"os"

	bt "codeberg.org/ssmns/shekan-go/pkg/beutify"
	"codeberg.org/ssmns/shekan-go/pkg/utils"
)

func Backup() bool {
	src := "/etc/resolv.conf"
	dest := "/etc/resolv.conf.bck"
	_, err := os.ReadFile(dest)
	if err != nil {
		bytesRead, err := os.ReadFile(src)
		if err != nil {
			log.Fatal(err)
		}
		err = os.WriteFile(dest, bytesRead, 0644)
		if err != nil {
			log.Fatal(err)
		}
		return true
	}
	return true
}

func Restore(init string) (string, bool) {
	dest := "/etc/resolv.conf"
	src := ""
	state := ""
	switch init {
	case "init":
		src = "/etc/resolv.conf.init"
		state = "init"
	case "bck", "backup", "back":
		src = "/etc/resolv.conf.bck"
		state = "previous backup"
	default:
		log.Fatal("You must select between : 'backup' or 'inint' for restore options ")
	}

	bytesRead, err := os.ReadFile(src)
	if err != nil {
		log.Fatal(err)
		return state, false
	}
	err = os.WriteFile(dest, bytesRead, 0644)
	if err == nil {
		return state, true
	} else {
		log.Fatal(err)
	}

	return state, false
}

func Show() {
	src := "/etc/resolv.conf"
	data, err := os.ReadFile(src)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print(string(data))
}

func Change(name string) {
	content := ""
	switch name {
	case "shecan":
		content = "nameserver 178.22.122.100 \n nameserver 185.51.200.2"
	case "begzar":
		content = "nameserver 185.55.226.26 \n nameserver 185.55.225.25"
	case "google":
		content = "nameserver 8.8.8.8 \n nameserver 8.8.4.4"
	case "403":
		content = "nameserver 10.202.10.202 \n nameserver 10.202.10.102"
	case "hostiran":
		content = "nameserver 172.29.0.100 \n nameserver 172.29.2.100"
	case "electrotm":
		content = "nameserver 78.157.42.101 \n nameserver 78.157.42.100"
	case "dnspro":
		content = "nameserver 185.105.236.236 \n nameserver 185.105.238.238"
	}

	fmt.Printf("%s We cannot guarantee the safety of these DNS servers and it is possible that providers do not prioritize your privacy. %s \n", bt.GetColor("red"), bt.GetColor("none"))
	sure := utils.GetPrompt()
	if !sure {
		log.Printf("%s Warning: %s Nothing was changed.", bt.GetColor("orange"), bt.GetColor("none"))
		return
	}
	if len(content) > 0 {
		f, err := os.Create("/etc/resolv.conf")
		if err != nil {
			log.Fatal(err)
		}
		_, err = f.WriteString(content + "\n")
		if err != nil {
			log.Fatal(err)
		}
		f.Sync()
	} else {

		log.Printf("%s Warning: %s Nothing was changed; becuse the %s'%s'%s is not valid or supported.\n  The supported DNs are:", bt.GetColor("orange"), bt.GetColor("none"), bt.GetColor("red"), name, bt.GetColor("none"))
		log.Printf("%s  shecan \n  begzar \n  google \n  403 \n  hostiran \n electrotm \n %sdnspro \n ", bt.GetColor("green"), bt.GetColor("none"))
	}
}
