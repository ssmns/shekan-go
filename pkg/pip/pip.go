package pip

import (
	"fmt"
	"log"
	"os/exec"

	bt "codeberg.org/ssmns/shekan-go/pkg/beutify"
)

func ChangePipProxy(name string) {
	server := ""
	switch name {
	case "china_01":
		server = "https://pypi.tuna.tsinghua.edu.cn/simple/ "
	case "china_02":
		server = "https://mirrors.aliyun.com/pypi/simple/ "
	case "china_03":
		server = "https://pypi.mirrors.ustc.edu.cn/simple/ "
	case "china_04":
		server = "https://repo.huaweicloud.com/repository/pypi/simple/ "
	case "china_05":
		server = "http://pypi.douban.com/simple/ "
	case "china_06":
		server = "http://pypi.sdutlinux.org/ "
	case "china_07":
		server = "http://pypi.hustunique.com/ "
	default:
		server = "https://pypi.tuna.tsinghua.edu.cn/simple/ "
	}

	out, err := exec.Command("pip3", "config", "set", "global.proxy", server, "--user").Output()

	if err != nil {
		fmt.Println(err)
		return
	}
	log.Printf("global.proxy was set to %s %s %s. For unset the pip proxy, run: \n %sshekan unsetpip%s.\n %s \n", bt.GetColor("blue"), server, bt.GetColor("none"), bt.GetColor("green"), bt.GetColor("none"), string(out))
}

func RestorePipProxy() {
	out, err := exec.Command("pip3", "config", "unset", "global.proxy", "--user").Output()

	if err != nil {
		fmt.Println(err)
		return
	}
	log.Printf("global.proxy was unset to default.\n %s \n", string(out))
}
